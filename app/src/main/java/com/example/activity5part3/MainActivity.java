package com.example.activity5part3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    Button btn_Start, btn_answerOption1Display, btn_answerOption2Display, btn_answerOption3Display, btn_answerOption4Display;
    TextView tv_timerDisplay, tv_pointDisplay, tv_currentQuestionDisplay, tv_status;
    ProgressBar prog_Timer;

    Game g = new Game();

    int secondsRemaining = 30;

    CountDownTimer timer = new CountDownTimer(30000,1000) {
        @Override
        public void onTick(long millisUntilFinished)
        {
            secondsRemaining--;
            tv_timerDisplay.setText(Integer.toString(secondsRemaining) + " Sec");
            prog_Timer.setProgress(30 - secondsRemaining);
        }

        @Override
        public void onFinish()
        {
            btn_answerOption1Display.setEnabled(false);
            btn_answerOption2Display.setEnabled(false);
            btn_answerOption3Display.setEnabled(false);
            btn_answerOption4Display.setEnabled(false);
            tv_status.setText("Time is up! " + g.getNumberCorrect() + "/" + (g.getTotalQuestions()-1));

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run()
                {
                    btn_Start.setVisibility(View.VISIBLE);
                }
            }, 4000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_Start = findViewById(R.id.btn_Start);
        btn_answerOption1Display = findViewById(R.id.btn_answerOption1Display);
        btn_answerOption2Display = findViewById(R.id.btn_answerOption2Display);
        btn_answerOption3Display = findViewById(R.id.btn_answerOption3Display);
        btn_answerOption4Display = findViewById(R.id.btn_answerOption4Display);

        tv_timerDisplay = findViewById(R.id.tv_timerDisplay);
        tv_pointDisplay = findViewById(R.id.tv_pointDisplay);
        tv_currentQuestionDisplay = findViewById(R.id.tv_currentQuestionDisplay);
        tv_status = findViewById(R.id.tv_status);

        prog_Timer = findViewById(R.id.prog_Timer);

        tv_timerDisplay.setText("0 Sec");
        tv_currentQuestionDisplay.setText("");
        tv_status.setText("Press Start");
        tv_pointDisplay.setText("0 PTS");

        View.OnClickListener startButtonClickListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Button start_button = (Button) v;

                secondsRemaining = 30;
                g = new Game();
                start_button.setVisibility(View.INVISIBLE);
                nextTurn();
                timer.start();
            }
        };

        View.OnClickListener answerButtonClickListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Button buttonClicked = (Button) v;

                int answerSelected = Integer.parseInt(buttonClicked.getText().toString());

                g.checkAnswer(answerSelected);
                tv_pointDisplay.setText(Integer.toString(g.getScore()));
                nextTurn();

            }
        };


        btn_Start.setOnClickListener(startButtonClickListener);

        btn_answerOption1Display.setOnClickListener(answerButtonClickListener);
        btn_answerOption2Display.setOnClickListener(answerButtonClickListener);
        btn_answerOption3Display.setOnClickListener(answerButtonClickListener);
        btn_answerOption4Display.setOnClickListener(answerButtonClickListener);
    }

    private void nextTurn()
    {
        g.makeNewQuestion();
        int[] answer = g.getCurrentQuestion().getAnswerArray();

        btn_answerOption1Display.setText(Integer.toString(answer[0]));
        btn_answerOption2Display.setText(Integer.toString(answer[1]));
        btn_answerOption3Display.setText(Integer.toString(answer[2]));
        btn_answerOption4Display.setText(Integer.toString(answer[3]));

        btn_answerOption1Display.setEnabled(true);
        btn_answerOption2Display.setEnabled(true);
        btn_answerOption3Display.setEnabled(true);
        btn_answerOption4Display.setEnabled(true);

        tv_currentQuestionDisplay.setText(g.getCurrentQuestion().getQuestionString());

        tv_status.setText(Integer.toString(g.getNumberCorrect()) + "/" + Integer.toString(g.getTotalQuestions()-1));
    }
}