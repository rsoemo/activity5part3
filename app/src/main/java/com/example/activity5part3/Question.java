package com.example.activity5part3;

import java.util.Random;

public class Question
{
    private int firstNumber;  //First Number
    private int secondNumber;  //Second number
    private String questionString;  //Question
    private int answer;  //Answer
    private int[] answerArray;  //Possible answers
    private int answerPosition;  //Where answer is located in possible answers
    private int upperLimit;  //How high can the answers be

    public Question(int upperLimit)
    {
        this.upperLimit = upperLimit;
        Random randomNumberMaker = new Random();

        this.firstNumber = randomNumberMaker.nextInt(upperLimit);
        this.secondNumber = randomNumberMaker.nextInt(upperLimit);
        this.answer = this.firstNumber + this.secondNumber;
        this.questionString = firstNumber + " + " + secondNumber + " = ";

        this.answerPosition = randomNumberMaker.nextInt(4);
        this.answerArray = new int[]{0,1,2,3};

        this.answerArray[0] = answer + 1;
        this.answerArray[1] = answer + 10;
        this.answerArray[2] = answer - 5;
        this.answerArray[3] = answer - 2;

        this.answerArray = shuffleArray(this.answerArray);

        answerArray[answerPosition] = answer;
    }

    private int[] shuffleArray(int[] array)
    {
        int index, temp;
        Random randomNumberGenerator = new Random();

        for (int i = array.length - 1; i > 0; i--)
        {
            index = randomNumberGenerator.nextInt(i+1);
            temp = array[index];
            array[index] = array[i];
            array[i] = temp;
        }
        return array;
    }

    public int getFirstNumber()
    {
        return firstNumber;
    }

    public void setFirstNumber(int firstNumber)
    {
        this.firstNumber = firstNumber;
    }

    public int getSecondNumber()
    {
        return secondNumber;
    }

    public void setSecondNumber(int secondNumber)
    {
        this.secondNumber = secondNumber;
    }

    public String getQuestionString()
    {
        return questionString;
    }

    public void setQuestionString(String questionString)
    {
        this.questionString = questionString;
    }

    public int getQuestionAnswer()
    {
        return answer;
    }

    public void setQuestionAnswer(int questionAnswer)
    {
        this.answer = questionAnswer;
    }

    public int[] getAnswerArray()
    {
        return answerArray;
    }

    public void setAnswerArray(int[] answerArray)
    {
        this.answerArray = answerArray;
    }

    public int getAnswerPosition()
    {
        return answerPosition;
    }

    public void setAnswerPosition(int answerPosition)
    {
        this.answerPosition = answerPosition;
    }

    public int getUpperLimit()
    {
        return upperLimit;
    }

    public void setUpperLimit(int upperLimit)
    {
        this.upperLimit = upperLimit;
    }
}
